dod_reserve

It is the spring of 1945 and the Allied forces are pushing ever further into Germany. Units of the US forces have stumbled across a secret German supply depot hidden deep in the hills. Defended by German armour and anti-aircraft guns, together with a complement of infantry, the Allies have to secure the complex thus depriving the Axis forces of much needed munitions and fuel.

Allied Objectives:
Capture the five flags located beside the Flak 18, Tiger Tank, Radio Room, Stug III and Freya Radar to secure victory.
Axis Objectives:
Defend all areas of the complex.


-------------------------------------------------------------------
- a map by HomerAlCartman aka Darkranger. visit hp @ www.s4ever.de

- credits:
moroes, kane, locololo2, cpt. ukulele, ranson, bigdog, thesurgeon -> models / prefabs / textures
Splatt from Rommels Renegades for elaborate testing, writing objectives text and finding bugs: visit www.rr-gamers.com
and to all others I have forgotten!